//const api_url = "<heroku_app_url>"
const api_url = "https://tcetlabapp.herokuapp.com/"

function loadData(records = []) {
    var table_data = "";
    for (let i = 0; i < records.length; i++) {
        table_data += `<tr>`;
        table_data += `<td>${records[i].name}</td>`;
        table_data += `<td>${records[i].ref_id}</td>`;
        table_data += `<td>${records[i].class}</td>`;
        table_data += `<td>${records[i].age}</td>`;
        table_data += `<td>${records[i].city}</td>`;
        table_data += `<td>${records[i].contact_no}</td>`;
        table_data += `<td>`;
        table_data += `<a href="edit.html?id=${records[i]._id}"><button class="btn btn-primary">Edit</button></a>`;
        table_data += '&nbsp;&nbsp;';
        table_data += `<button class="btn btn-danger" onclick=deleteData('${records[i]._id}')>Delete</button>`;
        table_data += `</td>`;
        table_data += `</tr>`;
    }
    //console.log(table_data);
    document.getElementById("tbody").innerHTML = table_data;
}

function getData() {
    fetch(api_url)
        .then((response) => response.json())
        .then((data) => {
            console.table(data);
            loadData(data);
        });
}


function getDataById(id) {
    fetch(`${api_url}/${id}`)
        .then((response) => response.json())
        .then((data) => {

            console.log(data);
            document.getElementById("id").value = data._id;
            document.getElementById("name").value = data.name;
            document.getElementById("ref_id").value = data.ref_id;
            document.getElementById("class").value = data.class;
            document.getElementById("age").value = data.age;
            document.getElementById("city").value = data.city;
            document.getElementById("contact_no").value = data.contact_no;
        })
}


function postData() {
    var name = document.getElementById("name").value;
    var ref_id = decument.getElementById("ref_id").value
    var class_ = decument.getElementById("class").value
    var age = document.getElementById("age").value;
    var city = document.getElementById("city").value;
    var contact_no = document.getElementById("contact_no").value;

    data = { name: name, ref_id: ref_id, class_ = class_, age: age, city: city, contact_no: contact_no };

    fetch(api_url, {
        method: "POST",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
        .then((response) => response.json())
        .then((data) => {
            console.log(data);
            window.location.href = "index.html";
        })
}


function putData() {

    var _id = document.getElementById("id").value;
    var ref_id = decument.getElementById("ref_id").value
    var class_ = decument.getElementById("class").value
    var age = document.getElementById("age").value;
    var city = document.getElementById("city").value;
    var contact_no = document.getElementById("contact_no").value;

    data = { _id: _id, name: name, ref_id: ref_id, class_ = class_, age: age, city: city, contact_no: contact_no };

    fetch(api_url, {
        method: "PUT",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
        .then((response) => response.json())
        .then((data) => {
            console.table(data);
            window.location.href = "index.html";
        })
}


function deleteData(id) {
    user_input = confirm("Are you sure you want to delete this record?");
    if (user_input) {
        fetch(api_url, {
            method: "DELETE",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ "_id": id })
        })
            .then((response) => response.json())
            .then((data) => {
                console.log(data);
                window.location.reload();
            })
    }
}